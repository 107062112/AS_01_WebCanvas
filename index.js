let draw; // draw:是否在繪圖
let canva, c;

let state = "";

let canvas, ctx;
let canvass, ct;
let tmp_r, tmp_g, tmp_b;
let grd;

let start_x, start_y, end_x, end_y, cur_x = 0, cur_y = 0, center_x, center_y, rad;

let undo_array = [];
let redo_array = [];
let drawing = false;
let first;

let filled = false;

let s = "";

let i;
function init() {
    canva = document.getElementById("canva");
    c = canva.getContext("2d");
    color_init();
    color_init2();
    let img = c.getImageData(0, 0, canva.width, canva.height);
    undo_array.push(img);
    i = document.getElementById('input');
    i.addEventListener('change', upload, false);
}

function color_init() {
    canvas = document.getElementById("color_plate");
    if (canvas.getContext) {
        ctx = canvas.getContext("2d");
        let gradient = ctx.createLinearGradient(0, 0, 300, 0);
        gradient.addColorStop(0, "rgb(255, 0, 0)");
        gradient.addColorStop(1 / 6, "rgb(255, 255, 0)");
        gradient.addColorStop(2 / 6, "rgb(0, 255, 0)");
        gradient.addColorStop(3 / 6, "rgb(0, 255, 255)");
        gradient.addColorStop(4 / 6, "rgb(0, 0, 255)");
        gradient.addColorStop(5 / 6, "rgb(255, 0, 255)");
        gradient.addColorStop(1, "rgb(255, 0, 0)");
        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, 300, 50);
    }
}

function color_init2(){
    canvass = document.getElementById("color_plate2");
    if (canvass.getContext) {
        ct = canvass.getContext("2d");
        let gradient2 = ct.createLinearGradient(0, 0, 300, 0);
        gradient2.addColorStop(0,"white");
        gradient2.addColorStop(0.5,"red");
        gradient2.addColorStop(1,"black");
        ct.fillStyle = gradient2;
        ct.fillRect(0, 0, 300, 50);
        tmp_r = 255;
        tmp_g = 0;
        tmp_b = 0;
    }
}

function color_change1(){
    let x = event.offsetX;
    let y = event.offsetY;
    ctx.clearRect(0, 0, 300, 50);
    color_init();
    ctx.beginPath();
    ctx.rect(x, y, 5, 5);
    ctx.stroke();
    ctx.closePath();
    let cnt = 256 * 6 / 300 * x;
    let turn = Math.floor(cnt / 256);
    s = "";
    if(turn == 0){
        s = "rgb(255," + cnt + ",0)";
    }else if(turn == 1){
        cnt -= 256;
        cnt = 256 - cnt;
        s = "rgb(" + cnt + ",255,0)";
        tmp_r = cnt; tmp_g = 255; tmp_b = 0;
    }else if(turn == 2){
        cnt -= 512;
        s = "rgb(0,255," + cnt + ")";
        tmp_r = 0; tmp_g = 255; tmp_b = cnt;
    }else if(turn == 3){
        cnt -= 768;
        cnt = 256 - cnt;
        s = "rgb(0," + cnt + ",255)";
        tmp_r = 0; tmp_g = cnt; tmp_b = 255;
    }else if(turn == 4){
        cnt -= 1024;
        s = "rgb(" + cnt + ",0,255)";
        tmp_r = cnt; tmp_g = 0; tmp_b = 255;
    }else if(turn == 5){
        cnt -= 1280;
        cnt = 256 - cnt;
        s = "rgb(255,0," + cnt + ")";
        tmp_r = 255; tmp_g = 0; tmp_b = cnt;
    }else{
        s = "rgb(255,0,0)";
        tmp_r = 255; tmp_g = 0; tmp_b = 0;
    }
    grd = ct.createLinearGradient(0, 0, 300, 0);
    grd.addColorStop(0,"rgb(255, 255, 255)");
    grd.addColorStop(0.5, s);
    grd.addColorStop(1,"rgb(0, 0, 0)");
    ct.fillStyle = grd;
    ct.fillRect(0, 0, 300, 50);
    c.strokeStyle = s;
}

function color_change2(){
    let x = event.offsetX;
    let y = event.offsetY;
    ct.clearRect(0, 0, 300, 50);
    ct.fillStyle = grd;
    ct.fillRect(0, 0, 300, 50);
    ct.beginPath();
    ct.rect(x, y, 5, 5);
    ct.stroke();
    ct.closePath();
    if(x <= 150){
        s = "";
        let rr = 255 - ((255 - tmp_r) * x / 150);
        let gg = 255 - ((255 - tmp_g) * x / 150);
        let bb = 255 - ((255 - tmp_b) * x / 150);
        s = "rgb(" + rr + "," + gg + "," + bb + ")";
        c.strokeStyle = s;
    }else{
        s = "";
        let rr = tmp_r - (tmp_r * (x - 150) / 150);
        let gg = tmp_g - (tmp_g * (x - 150) / 150);
        let bb = tmp_b - (tmp_b * (x - 150) / 150);
        s = "rgb(" + rr + "," + gg + "," + bb + ")";
        c.strokeStyle = s;
    }
}

function undo() {
    if (undo_array.length == 1) {
        undo_array.pop();
        c.clearRect(0, 0, canva.width, canva.height);
        let img = c.getImageData(0, 0, canva.width, canva.height);
        undo_array.push(img);
    } else if (undo_array.length > 1) {
        if (drawing == true) {
            while (redo_array.length > 0) redo_array.pop();
        }
        drawing = false;
        first = true;
        redo_array.push(undo_array[undo_array.length - 1]);
        let img = undo_array[undo_array.length - 2];
        c.putImageData(img, 0, 0);
        undo_array.pop();
    }
}

function redo() {
    if (drawing == false) {
        if (redo_array.length == 1) {
            if (first) undo_array.push(redo_array[redo_array.length - 1]);
            first = false;
            c.putImageData(redo_array[0], 0, 0);
        } else if (redo_array.length > 1) {
            first = true;
            undo_array.push(redo_array[redo_array.length - 1]);
            c.putImageData(redo_array[redo_array.length - 1], 0, 0);
            redo_array.pop();
        }
    }
}

function penWidth_change(value) { // change penWidth
    c.lineWidth = value;
}

function wordSkin_change(value) {
    return value;
}

let x, y;

function put_text() {
    $("#canva_div").click(function (e) {
        if (state == "text") {
            let t = $('<input id="box" type="text">')
                .css({
                    left: e.pageX + 'px',
                    top: e.pageY + 'px'
                })
                .appendTo(document.body);
            x = e.pageX;
            y = e.pageY;
            state = "text_fin";
        }
    });
}

function draw_text() {
    document.body.addEventListener('click', function () {
        let box = document.getElementById("box");
        let v = document.getElementById("box").value;
        if (v != "" && state == "text_fin") {
            c.font = document.getElementById("penWidth").value + 'px ' + document.getElementById("wordSkin").value;
            c.fillStyle = s;
            c.fillText(v, x - 150, y - 130);
            box.remove();
            mouseup();
            state = "text";
        }
    });
}

function mousedown() {
    if (state == "pencil" || state == "eraser") {
        c.moveTo(event.offsetX, event.offsetY);
        draw = true;
        c.beginPath();
    } else if (state == "circle" || state == "square" || state == "triangle") {
        start_x = event.offsetX;
        start_y = event.offsetY;
        draw = true;
    } else if (state == "text") {
        put_text();
    } else if (state == "text_fin") {
        draw_text();
    }
}

function mousemove() {
    if (draw && state == "pencil") {
        c.lineCap = "round";
        c.lineTo(event.offsetX, event.offsetY);
        c.stroke();
    } else if (draw && state == "eraser") {
        c.lineCap = "round";
        c.globalCompositeOperation = 'destination-out';
        c.lineTo(event.offsetX, event.offsetY);
        c.stroke();
    } else if (draw && state == "circle") {
        c.clearRect(0, 0, canva.width, canva.height);
        if (undo_array.length > 0) {
            let img = undo_array[undo_array.length - 1];
            //undo_array.pop();
            c.putImageData(img, 0, 0);
        }
        cur_x = event.offsetX;
        cur_y = event.offsetY;
        center_x = (start_x + cur_x) / 2;
        center_y = (start_y + cur_y) / 2;
        rad = Math.sqrt((start_x - cur_x) * (start_x - cur_x) + (start_y - cur_y) * (start_y - cur_y)) / 2;
        c.beginPath();
        c.arc(center_x, center_y, rad, 0, 2 * Math.PI);
        c.fillStyle = s;
        if (filled) c.fill();
        c.stroke();
    } else if (draw && state == "square") {
        c.clearRect(0, 0, canva.width, canva.height);
        if (undo_array.length > 0) {
            let img = undo_array[undo_array.length - 1];
            //undo_array.pop();
            c.putImageData(img, 0, 0);
        }
        cur_x = event.offsetX;
        cur_y = event.offsetY;
        c.beginPath();
        c.lineCap = "round";
        if (filled){
            c.fillStyle = s;
            c.fillRect(start_x, start_y, cur_x - start_x, cur_y - start_y);
        }else c.rect(start_x, start_y, cur_x - start_x, cur_y - start_y);
        c.stroke();
    } else if (draw && state == "triangle") {
        c.clearRect(0, 0, canva.width, canva.height);
        if (undo_array.length > 0) {
            let img = undo_array[undo_array.length - 1];
            //undo_array.pop();
            c.putImageData(img, 0, 0);
        }
        cur_x = event.offsetX;
        cur_y = event.offsetY;
        c.beginPath();
        c.lineCap = "round";
        c.moveTo((start_x + cur_x) / 2, start_y);
        c.lineTo(cur_x, cur_y);
        c.lineTo(start_x, cur_y);
        c.fillStyle = s;
        if (filled) c.fill();
        c.closePath();
        c.stroke();
    }
}

function mouseup() {
    if (state != "") {
        let img = c.getImageData(0, 0, 1000, 400);
        undo_array.push(img);
        drawing = true;
    }
    console.log(undo_array.length);
    if (state == "pencil" || state == "eraser") {
        draw = false;
        c.closePath();
    } else if (state == "circle") {
        c.closePath();
        draw = false;
    } else if (state == "square") {
        draw = false;
        c.closePath();
    } else if (state == "triangle") {
        draw = false;
    }
}


function pencil_click() { // pencil
    state = "pencil";
    canva.style.cursor = 'url(Large_Pastel_Green.cur), auto';
    c.globalCompositeOperation = 'source-over';
}

function eraser_click() { // eraser
    state = "eraser";
    canva.style.cursor = 'url(Eraser.cur), auto';
}

function text_click() { // text
    state = "text";
    canva.style.cursor = 'url(text.cur), auto';
    c.globalCompositeOperation = 'source-over';
}

function draw_circle() { // circle
    state = "circle";
    canva.style.cursor = 'url(Circle.cur), auto';
    c.globalCompositeOperation = 'source-over';
}

function draw_square() { // square
    state = "square";
    canva.style.cursor = 'url(Square.cur), auto';
    c.globalCompositeOperation = 'source-over';
}

function draw_triangle() { // triangle
    state = "triangle";
    canva.style.cursor = 'url(Illuminati.cur), auto';
    c.globalCompositeOperation = 'source-over';
}

function refresh_click() { // refresh
    c.putImageData(undo_array[0], 0, 0);
    while (undo_array.length != 1) undo_array.pop();
    while (redo_array.length != 0) redo_array.pop();
}

function fills() { // filled
    if (filled == false) filled = true;
    else filled = false;
}

function upload(e) {
    let reader = new FileReader();
    reader.onload = (event) => {
        let img = new Image();
        img.onload = function () {
            if (img.width > 1000) img.width = 1000 + 'px';
            if (img.height > 400) img.height = 400 + 'px';
            c.drawImage(img, 0, 0);
            state = "img";
            mouseup();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}




